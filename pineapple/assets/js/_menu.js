class Menu {
    constructor () {
        this.mainMenuClass = 'main-menu';
        this.mainMenu = $(`.${this.mainMenuClass}`);
        this.toggler = $(`.${this.mainMenuClass}__toggler`);
        this.isOpen = false;
    }

    init() {
        const { toggler, mainMenu } = this;

        toggler.click(() => {
            this.isOpen = !this.isOpen;

            mainMenu.toggleClass(`${this.mainMenuClass}--open`);
            $('body').toggleClass('frozen-pineapple');

            if (this.isOpen) {
                $.scrollify.disable();
            } else {
                $.scrollify.enable();
            }
        });
    }
}

/* exported Menu */
